# [5.1.0](https://gitlab.com/gitlab-org/frontend/gettext-extractor-vue/compare/v5.0.0...v5.1.0) (2023-03-22)


### Features

* Add supports for an optional guard ([6cbeddc](https://gitlab.com/gitlab-org/frontend/gettext-extractor-vue/commit/6cbeddc988baed25ec38d1ee051666b97aa20fd5))

# [5.0.0](https://gitlab.com/gitlab-org/frontend/gettext-extractor-vue/compare/v4.0.2...v5.0.0) (2021-02-14)


### Features

* Add support for parsing Vue@3 Single File Components ([ac4ea80](https://gitlab.com/gitlab-org/frontend/gettext-extractor-vue/commit/ac4ea803785402799a561d382b666c5dd4753b85))


### BREAKING CHANGES

* A template compiler needs to be provided to
`decorateJSParserWithVueSupport`. So in order to migrate change:

```js
decorateJSParserWithVueSupport(jsParser)
```

to be:

```js
decorateJSParserWithVueSupport(jsParser, { vue2TemplateCompiler: require('vue-template-compiler') })
```
