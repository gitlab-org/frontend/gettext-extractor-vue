const extractVue2Snippets = (file, filename, compiler) => {
  const source = compiler.parseComponent(file, { pad: 'line' });

  const snippets = [];

  if (source.template && source.template.content) {
    const count = (file.substr(0, source.template.start + 1).match(/[\r\n]/g) || []).length;

    const compiled = compiler.compile(source.template.content);

    snippets.push({
      code: compiled.render,
      filename,
      line: count + 1,
    });

    compiled.staticRenderFns.forEach((fn) => {
      snippets.push({
        code: fn,
        filename,
        line: count + 1,
      });
    });
  }

  if (source.script && source.script.content) {
    const scriptSnippet = {
      code: source.script.content,
      filename,
      line: 1,
    };

    if (snippets.length && source.script.start < source.template.start) {
      snippets.unshift(scriptSnippet);
    } else {
      snippets.push(scriptSnippet);
    }
  }

  return snippets;
};

module.exports = extractVue2Snippets;
