const test = require('ava');
const path = require('path');
const glob = require('glob');
const fs = require('fs');

const { decorateJSParserWithVueSupport } = require('../../index');

const root = path.resolve(__dirname + '/__fixtures__/');

const vueFixtures = glob.sync('*.vue', {
  cwd: root,
  absolute: true,
});

const jsFixtures = glob.sync('*.js', {
  cwd: root,
  absolute: true,
});

const { GettextExtractor, JsExtractors } = require('gettext-extractor');

const initExtractor = (options = { vue2TemplateCompiler: require('vue-template-compiler') }) => {
  const extractor = new GettextExtractor();

  const jsParser = extractor.createJsParser([
    // Place all the possible expressions to extract here:
    JsExtractors.callExpression('__', {
      arguments: {
        text: 0,
      },
    }),
    JsExtractors.callExpression('n__', {
      arguments: {
        text: 0,
        textPlural: 1,
      },
    }),
    JsExtractors.callExpression('s__', {
      arguments: {
        text: 0,
      },
    }),
  ]);

  return { extractor, vueParser: decorateJSParserWithVueSupport(jsParser, options) };
};

const cleanText = (text) => (text ? text.replace(/[\r\n] */g, '\n ').trim() : text);

const cleanResult = (messages) => {
  return messages.map(({ references, text, textPlural, ...rest }) => {
    return {
      ...rest,
      text: cleanText(text),
      textPlural: cleanText(textPlural),
      references: new Set(references.map((x) => path.basename(x).replace(/:\d+$/, ''))),
    };
  });
};

const matrix = {
  vue2: { vue2TemplateCompiler: require('vue-template-compiler') },
  vue3: { vue3TemplateCompiler: require('@vue/compiler-sfc') },
  vue2Guarded: { vue2TemplateCompiler: require('vue-template-compiler'), guard: '__' },
  vue3Guarded: { vue3TemplateCompiler: require('@vue/compiler-sfc'), guard: '__' },
};

Object.entries(matrix).forEach(([version, options]) => {
  test(`(${version}) parseString: should parse singular strings correctly`, async (t) => {
    const { vueParser, extractor } = initExtractor(options);

    await vueParser.parseString('s__("this")', 'a.js');
    t.deepEqual(extractor.getMessages(), [
      {
        text: 'this',
        textPlural: null,
        context: null,
        references: ['a.js:1'],
        comments: [],
      },
    ]);
  });

  test(`(${version}) parseString: should parse plural strings correctly`, async (t) => {
    const { vueParser, extractor } = initExtractor(options);

    await vueParser.parseString('n__("text", "texts")', 'a.js');
    t.deepEqual(extractor.getMessages(), [
      {
        text: 'text',
        textPlural: 'texts',
        context: null,
        references: ['a.js:1'],
        comments: [],
      },
    ]);
  });

  [...vueFixtures, ...jsFixtures].forEach((file) => {
    const baseName = path.basename(file);
    test(`(${version}) parseFile: should parse ${baseName} correctly`, async (t) => {
      const { vueParser, extractor } = initExtractor();

      const comparison = JSON.parse(fs.readFileSync(root + '/' + baseName + '.json', 'utf8'));

      await vueParser.parseFile(file);

      t.deepEqual(cleanResult(extractor.getMessages()), cleanResult(comparison));
    });
  });

  test(`(${version}) parseFileGlob: should parse all files matching the 'fixtures/*.{js,vue}' glob`, async (t) => {
    const { vueParser, extractor } = initExtractor();

    const comparison = JSON.parse(fs.readFileSync(root + '/all.vue.json', 'utf8'));

    await vueParser.parseFilesGlob('*.{js,vue}', { cwd: root, absolute: true });

    t.deepEqual(cleanResult(extractor.getMessages()), cleanResult(comparison));
  });
});
