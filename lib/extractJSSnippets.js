const extractJSSnippets = (code, filename) => {
  const snippets = [];
  snippets.push({
    code,
    filename,
    line: 1,
  });

  return snippets;
};

module.exports = extractJSSnippets;
