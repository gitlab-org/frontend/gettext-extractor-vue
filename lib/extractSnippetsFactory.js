const extractVue2Snippets = require('./extractVue2Snippets');
const extractVue3Snippets = require('./extractVue3Snippets');
const extractJSSnippets = require('./extractJSSnippets');
const fs = require('fs').promises;

const extractSnippetsFactory = ({
  vue2TemplateCompiler = null,
  vue3TemplateCompiler = null,
  guard = null,
} = {}) => {
  if (vue2TemplateCompiler && vue3TemplateCompiler) {
    throw new Error(
      'Please specify just vue2TemplateCompiler _or_ vue3TemplateCompiler as an option. These are mutually exclusive.'
    );
  }

  if (!vue2TemplateCompiler && !vue3TemplateCompiler) {
    throw new Error(
      'Please provide a template compiler via the vue2TemplateCompiler _or_ vue3TemplateCompiler option.'
    );
  }

  const USE_VUE_2 = Boolean(vue2TemplateCompiler);

  return async (file) => {
    const content = await fs.readFile(file, 'utf-8');
    if (guard && content.indexOf(guard) < 0) {
      return [];
    }
    if (file.endsWith('vue')) {
      return USE_VUE_2
        ? extractVue2Snippets(content, file, vue2TemplateCompiler)
        : extractVue3Snippets(content, file, vue3TemplateCompiler);
    }
    return extractJSSnippets(content, file);
  };
};

module.exports = extractSnippetsFactory;
