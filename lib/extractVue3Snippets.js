const extractVue3Snippets = (file, filename, compiler) => {
  const { descriptor } = compiler.parse(file, { pad: 'line', filename });

  const snippets = [];

  if (descriptor.template && descriptor.template.content) {
    const compiled = compiler.compileTemplate({
      source: descriptor.template.content,
      filename,
      id: filename,
    });

    snippets.push({
      code: compiled.code.replace(/_ctx\./g, ''),
      filename,
      line: descriptor.template.loc.start.line + 1,
    });
  }

  if (descriptor.script && descriptor.script.content) {
    const scriptSnippet = {
      code: descriptor.script.content,
      filename,
      line: descriptor.script.loc.start.line,
    };

    if (snippets.length && descriptor.script.loc.start.line < descriptor.template.loc.start.line) {
      snippets.unshift(scriptSnippet);
    } else {
      snippets.push(scriptSnippet);
    }
  }

  return snippets;
};

module.exports = extractVue3Snippets;
