# gettext-extractor-vue

> Extend [gettext-extractor](https://www.npmjs.com/package/gettext-extractor) with the possibility to parse `.vue` single file components.

## Usage: Adding Vue SFC Support with `decorateJSParserWithVueSupport`

Support for Vue single file components (SFC) is provided by a decorating the `JSExtractor` of `gettext-extractor`.
The Single File Components will be transformed to JavaScript and fed into `JSExtractor`.
Both vue@2 (via `vue-template-compiler`) and vue@3 (via `@vue/compiler-sfc`) are supported.

Below you can see an example, derived from `gettext-extractor`'s README.

```js
const { GettextExtractor, JsExtractors } = require('gettext-extractor');
const { decorateJSParserWithVueSupport } = require('gettext-extractor-vue');

const extractor = new GettextExtractor();

const jsParser = extractor.createJsParser([
  JsExtractors.callExpression('getText', {
    arguments: {
      text: 0,
      context: 1,
    },
  }),
  JsExtractors.callExpression('getPlural', {
    arguments: {
      text: 1,
      textPlural: 2,
      context: 3,
    },
  }),
]);

let vueParser;

// For vue@2 support please provide vue-template-compiler via `vue2TemplateCompiler`
vueParser = decorateJSParserWithVueSupport(jsParser, {
  vue2TemplateCompiler: require('vue-template-compiler'),
});
// For vue@3 support please provide @vue/compiler-sfc via `vue3TemplateCompiler`
vueParser = decorateJSParserWithVueSupport(jsParser, {
  vue3TemplateCompiler: require('@vue/compiler-sfc'),
});

vueParser.parseFilesGlob('./src/**/*.@(js|vue)');

extractor.savePotFile('./messages.pot');

extractor.printStats();
```

## Usage: Transforming messages with `decorateExtractorWithHelpers`

`decorateExtractorWithHelpers` enables to you to add message transform functions to the extractor.

For example:

```js
const { GettextExtractor, JSExtractor } = require('gettext-extractor');
const { decorateExtractorWithHelpers } = require('gettext-extractor-vue');

const extractor = decorateExtractorWithHelpers(new GettextExtractor());

// Ensure that every message has leading and trailing spaces removed
extractor.addMessageTransformFunction((message) => message.trim());
```
